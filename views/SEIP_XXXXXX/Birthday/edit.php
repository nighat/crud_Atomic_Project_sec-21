<?php

require_once("../../../vendor/autoload.php");
if(!isset($_SESSION)) session_start();
use App\Message\Message;

$msg = Message::message();

echo "<div>  <div id='message'>  $msg </div>   </div>";

$objBirthday = new \App\Birthday\Birthday();
$objBirthday->setData($_GET);
$oneData = $objBirthday->view();

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Birthday Add Form</title>
</head>
<body>
<h2 style="text-align:center ">Birthday Add Form</h2>
<form action="update.php" method="post">
    <table border="1"; width=550 align=center bgcolor="#8fbc8f" >
        <tr><td>Please Enter Name:</td>
            <td> <input type="text" name="name" value="<?php echo $oneData->name?>"></td></tr>
        <tr>
            <td>Please Enter Birthday Date:</td>
            <td><input type="date" name="date"value="<?php echo $oneData->date?>"></td></tr>
        <tr><td><input type="hidden" name="id"value="<?php echo $oneData->id?>"></td></tr>

        <tr><td colspan="2"> <input type="submit" value="update"></td></tr>
    </table>

</form>
<script src="../../../resource/bootstrap/js/jquery.js"></script>

<script>


    jQuery(

        function($) {
            $('#message').fadeOut (550);
            $('#message').fadeIn (550);
            $('#message').fadeOut (550);
            $('#message').fadeIn (550);
            $('#message').fadeOut (550);
            $('#message').fadeIn (550);
            $('#message').fadeOut (550);
        }
    )
</script>

</body>
</html>
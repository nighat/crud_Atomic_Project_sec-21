<?php

namespace App\Birthday;

use App\Message\Message;
use App\Utility\Utility;


use App\Model\Database as DB;
use PDO;

class Birthday extends DB
{

    private $id;
    private $name;
    private $date;



    public function setData($postData){

        if(array_key_exists('id',$postData)){
            $this->id = $postData['id'];
        }

        if(array_key_exists('name',$postData)){
            $this->name = $postData['name'];
        }

        if(array_key_exists('date',$postData)){
            $this->date = $postData['date'];
        }

    }


    public function store(){

        $arrData = array($this->name,$this->date);

        $sql = "INSERT into Birthday(name,date) VALUES(?,?)";

        $STH = $this->DBH->prepare($sql);

        $result =$STH->execute($arrData);

        if($result)
            Message::message("Success! Data Has Been Inserted Successfully :)");
        else
            Message::message("Failed! Data Has Not Been Inserted Successfully :( ");

        Utility::redirect('index.php');


    }



    public function index(){

        $sql = "select * from Birthday where soft_deleted='No'";

        $STH = $this->DBH->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        return $STH->fetchAll();

    }


    public function view(){

        $sql = "select * from Birthday where id=".$this->id;

        $STH = $this->DBH->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        return $STH->fetch();

    }


    public function trashed(){

        $sql = "select * from Birthday where soft_deleted='Yes'";

        $STH = $this->DBH->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        return $STH->fetchAll();

    }
    public function update(){

        $arrData = array($this->name,$this->date);

        $sql = "UPDATE Birthday SET name=?,date=? WHERE id=".$this->id;

        $STH = $this->DBH->prepare($sql);

        $result =$STH->execute($arrData);

        if($result)
            Message::message("Success! Data Has Been Updated Successfully :)");
        else
            Message::message("Failed! Data Has Not Been Updated Successfully :( ");

        Utility::redirect('index.php');


    }
    public function trash(){

        $sql = "UPDATE Birthday SET soft_deleted='Yes' WHERE id=".$this->id;
        $result= $this->DBH->exec($sql);

        if($result)
            Message::message("Success! Data Has Been Soft_Deleted Successfully :)");
        else
            Message::message("Failed! Data Has Not Been Soft_Deleted Successfully :( ");

        Utility::redirect('index.php');


    }
    public function recover(){

        $sql = "UPDATE Birthday SET soft_deleted='No' WHERE id=".$this->id;
        $result= $this->DBH->exec($sql);

        if($result)
            Message::message("Success! Data Has Been Recovered Successfully :)");
        else
            Message::message("Failed! Data Has Not Been Recovered Successfully :( ");

        Utility::redirect('index.php');


    }
    public function delete(){

        $sql = "Delete from Birthday WHERE id=".$this->id;

        $result = $this->DBH->exec($sql);


        if($result)
            Message::message("Success! Data Has Been Permanently Deleted :)");
        else
            Message::message("Failed! Data Has Not Been Permanently Deleted  :( ");


        Utility::redirect('index.php');


    }
    public function indexPaginator($page=1,$itemsPerPage=3){

        $start = (($page-1) * $itemsPerPage);

        $sql = "SELECT * from Birthday  WHERE soft_deleted = 'No' LIMIT $start,$itemsPerPage";

        $STH = $this->DBH->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        $arrSomeData  = $STH->fetchAll();
        return $arrSomeData;

    }





}
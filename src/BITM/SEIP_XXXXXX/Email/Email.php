<?php

namespace App\Email;

use App\Message\Message;
use App\Utility\Utility;


use App\Model\Database as DB;
use PDO;

class  Email  extends DB
{

    private $id;
    private $name;
    private $email ;



    public function setData($postData){

        if(array_key_exists('id',$postData)){
            $this->id = $postData['id'];
        }

        if(array_key_exists('name',$postData)){
            $this->name = $postData['name'];
        }

        if(array_key_exists('email',$postData)){
            $this->email = $postData['email'];
        }

    }


    public function store(){

        $arrData = array($this->name,$this->email );

        $sql = "INSERT into email(name,email) VALUES(?,?)";

        $STH = $this->DBH->prepare($sql);

        $result =$STH->execute($arrData);

        if($result)
            Message::message("Success! Data Has Been Inserted Successfully :)");
        else
            Message::message("Failed! Data Has Not Been Inserted Successfully :( ");

        Utility::redirect('index.php');


    }



    public function index(){

        $sql = "select * from email where soft_deleted='No'";

        $STH = $this->DBH->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        return $STH->fetchAll();

    }


    public function view(){

        $sql = "select * from email where id=".$this->id;

        $STH = $this->DBH->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        return $STH->fetch();

    }


    public function trashed(){

        $sql = "select * from email where soft_deleted='Yes'";

        $STH = $this->DBH->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        return $STH->fetchAll();

    }
    public function update(){

    $arrData = array($this->name,$this->email );

    $sql = "UPDATE email SET name=?,email=? WHERE id=".$this->id;

    $STH = $this->DBH->prepare($sql);

    $result =$STH->execute($arrData);

    if($result)
        Message::message("Success! Data Has Been updated Successfully :)");
    else
        Message::message("Failed! Data Has Not Been updated Successfully :( ");

    Utility::redirect('index.php');


}
    public function trash(){

    $sql = "UPDATE email SET soft_deleted='Yes' WHERE id=".$this->id;

    $result = $this->DBH->exec($sql);


    if($result)
        Message::message("Success! Data Has Been updated Successfully :)");
    else
        Message::message("Failed! Data Has Not Been updated Successfully :( ");

    Utility::redirect('index.php');


}
    public function recover(){

        $sql = "UPDATE email SET soft_deleted='No' WHERE id=".$this->id;

        $result = $this->DBH->exec($sql);


        if($result)
            Message::message("Success! Data Has Been Soft_Deleted Successfully :)");
        else
            Message::message("Failed! Data Has Not Been Soft_DeletedSuccessfully :( ");

        Utility::redirect('index.php');


    }
    public function delete(){

        $sql = "Delete from email WHERE id=".$this->id;

        $result = $this->DBH->exec($sql);



        if($result)
            Message::message("Success! Data Has Been Permanently Deleted :)");
        else
            Message::message("Failed! Data Has Not Been Permanently Deleted  :( ");


        Utility::redirect('index.php');


    }

    public function indexPaginator($page=1,$itemsPerPage=3){

        $start = (($page-1) * $itemsPerPage);

        $sql = "SELECT * from Email  WHERE soft_deleted = 'No' LIMIT $start,$itemsPerPage";

        $STH = $this->DBH->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        $arrSomeData  = $STH->fetchAll();
        return $arrSomeData;

    }





}
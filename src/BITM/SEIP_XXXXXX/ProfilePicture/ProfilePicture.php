<?php


namespace App\ProfilePicture;
use App\Model\Database as DB;
use App\Message\Message;
use App\Utility\Utility;
use PDO;


class ProfilePicture extends DB
{
    private $id;
    private $name;
    private $fileName;
    private $tmpName;



    public function setData($postData) {
        if(array_key_exists('id',$postData)) {
            $this->id = $postData['id'];
        }


        if(array_key_exists('name',$postData)) {
            $this->name = $postData['name'];

        }

        if(array_key_exists('file_upload',$postData)){
            if(array_key_exists("name",$postData)){
                $this->fileName = $postData['file_upload']['name'];
            }
        }
        if(array_key_exists('file_upload',$postData)){
            if(array_key_exists("name",$postData)){
                $this->tmpName = $postData['file_upload']['tmp_name'];
            }
        }
    }

    public function store() {

        $arrData = array($this->name,$this->fileName);
        $sql = "INSERT INTO profile_picture(name,file_name) VALUES (?,?)";
        $statement = $this->DBH->prepare($sql);
        $result = $statement->execute($arrData);



        if($result)
            Message::message("Success! Data Has Been Inserted Successfully :)");
        else
            Message::message("Failed! Data Has Not Been Inserted Successfully :( ");

        Utility::redirect('index.php');

    }

    public function moveFile() {
        move_uploaded_file($this->tmpName, "uploads/".$this->fileName);
    }

    public function index(){

        $sql = "select * from profile_picture where soft_deleted='No'";

        $STH = $this->DBH->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        return $STH->fetchAll();

    }


    public function view(){

        $sql = "select * from profile_picture where id=".$this->id;

        $STH = $this->DBH->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        return $STH->fetch();

    }


    public function trashed(){

        $sql = "select * from profile_picture where soft_deleted='Yes'";

        $STH = $this->DBH->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        return $STH->fetchAll();

    }
    public function update() {

        $arrData = array($this->name,$this->fileName);
        $sql = "UPDATE profile_picture SET name=?,file_name=? WHERE id=".$this->id;
        $statement = $this->DBH->prepare($sql);
        $result = $statement->execute($arrData);



        if($result)
            Message::message("Success! Data Has Been updated Successfully :)");
        else
            Message::message("Failed! Data Has Not Been updated Successfully :( ");

        Utility::redirect('index.php');

    }
    public function trash() {


        $sql = "UPDATE profile_picture SET soft_deleted='Yes' WHERE id=".$this->id;
        $result  = $this->DBH->exec($sql);

        if($result)
            Message::message("Success! Data Has Been Soft_Deleted Successfully :)");
        else
            Message::message("Failed! Data Has Not Been Soft_Deleted Successfully :( ");

        Utility::redirect('index.php');

    }
    public function recover() {


        $sql = "UPDATE profile_picture SET soft_deleted='Yes' WHERE id=".$this->id;
        $result  = $this->DBH->exec($sql);

        if($result)
            Message::message("Success! Data Has Been Recovered  Successfully :)");
        else
            Message::message("Failed! Data Has Not Been Recovered Successfully :( ");

        Utility::redirect('index.php');

    }

    public function delete(){

        $sql = "Delete from profile_picture WHERE id=".$this->id;

        $result = $this->DBH->exec($sql);



        if($result)
            Message::message("Success! Data Has Been Permanently Deleted :)");
        else
            Message::message("Failed! Data Has Not Been Permanently Deleted  :( ");


        Utility::redirect('index.php');


    }

    public function indexPaginator($page=1,$itemsPerPage=3){

        $start = (($page-1) * $itemsPerPage);

        $sql = "SELECT * from profile_picture WHERE soft_deleted = 'No' LIMIT $start,$itemsPerPage";

        $STH = $this->DBH->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        $arrSomeData  = $STH->fetchAll();
        return $arrSomeData;

    }


}